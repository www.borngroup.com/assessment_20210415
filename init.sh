#!/bin/sh

echo "Setting up docker network ..."  && \
docker network create proxy_network_20210415
echo "Setting up environment files (.env) ..."  && \
rm -rf .env && cp -rp .env.example .env 
echo "docker-compose up -d ..."  && \
docker-compose up -d
echo "composer install ..."  && \
docker-compose exec --user www-data app bash -c "composer install" 
echo "initialize complete..."
sleep 10
sh init_db.sh

