# Assessment 20210415

## [0.1-0.8] Setup Dev Env

- 0.1) setup docker services (apache - points to public/index.php
,mysql -> born database, adminer - points -> mysql) by create file docker-compose.yml
- 0.2) add .env.example link to empty config, add .gitignore to ignore sensitive configs eg: .env, vendor
- 0.3) create .docker directory (keep all docker related files)
- 0.4) create init.sh to initialize docker,composer,db etc
- 0.5) create shell.sh access php server to run any composer commands
- 0.6) create app folder (system), public/index.php (nginx to call)
- 0.7) test sh init.sh if it works (public/index.php show hello world & mysql connected) 
- 0.8) allow php to listen .env file by composer require ... (for security on config)

## [0.9.1-0.9.5] Create from scratch a base that uses OOP MVC structure
- 0.9.1) public's folder structure:
public--index.php (points to app/all.php)
- 0.9.2) app's folder structure:
```
app--lib/Core.php
app--lib/Database.php
app--lib/Controller.php
app--config/config.php
app--m/User.php
app--v/index.php
app--c/Pages.php
app--all.php

```
- 0.9.3) make mvc works like (domain/controller/method/param/param)
- 0.9.4) add Rest Controller, index method, try hello world 
- 0.9.5) test mysql connections

## [0.9.6] RULES
- 0.9.6.1) Use MySQL database to fetch information. 
- ANS: (observe - 0.1, 0.7, 0.9.5)
- 0.9.6.2) Use proper error handling 
- ANS: (observe - 1.6 validation - Added status 400 Bad Request)
- 0.9.6.3)  Use PHP Classes, Methods, Members  
- ANS: (observe - 0.9.2 SETUP MVC)
- 0.9.6.4) Use proper Coding standards 
- ANS: (observe - 0.9.x SETUP MVC)

## [1.1-1.4] Create from scratch a base that uses OOP MVC structure 
- 1.1) add born.sql to .docker/init/mysql (order,customer,items)
- 1.2) restart mysql by: docker-compose down --volume && sh init.sh to re-insert born.sql
- 1.3) adminer to insert dummy datas to orders,customers,items,order_items
- 1.4) http://<domain>/rest/order/id/<orderid>
- 1.6) add validation if (orderid) not filled (RULE - 0.9.6.2)


## [conclusion] 
- (pre-requisite) to test this: machine has to have (docker+docker-compose)
```
sh init.sh
```
- able to test this system with:
- http://localhost:10001/rest/order/id/1 <-- success with json
- http://localhost:10001/rest/order/id/2 <-- success with json
- http://localhost:10001/rest/order/id/ <-- 400 bad request
- http://localhost:10001/rest/order/ <-- 400 bad request
- http://localhost:10001/rest/ <-- 200 OK