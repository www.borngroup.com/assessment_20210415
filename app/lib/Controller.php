<?php
    //Load the model and the view
    class Controller {
        public function model($model) {
            //Require model file
            require_once PATH_MODEL . $model . '.php';
            //Instantiate model
            return new $model();
        }

        //Load the view (checks for the file)
        public function view($view, $data = []) {
            if (file_exists(PATH_VIEW . $view . '.php')) {
                require_once PATH_VIEW . $view . '.php';
            } else {
                die("View does not exists.");
            }
        }
    }
