<?php
    class Customers {
        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        /* Test (database and table needs to exist before this works)
        */
        public function get() {
            $this->db->query("SELECT * FROM customers");

            $result = $this->db->resultSet();

            return $result;
        }

        public function getCustByID($id = null) {

            if(empty($id)) {
                return '';
            }
            $this->db->query("SELECT * FROM customers WHERE id = $id");
            $result = $this->db->resultSet();
            return $result;
        }
    }
