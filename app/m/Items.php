<?php
    class Items {
        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        /* Test (database and table needs to exist before this works)
        */
        public function get() {
            
            $this->db->query("SELECT * FROM items");

            $result = $this->db->resultSet();

            return $result;
        }

        public function getItemsByID($id = null) {

            if(empty($id)) {
                return '';
            }
            $this->db->query("SELECT * FROM items WHERE id = $id");
            $result = $this->db->resultSet();
            return $result;
        }
        

        public function getItemsByOrderID($id = null) {
            if(empty($id)) {
                return '';
            }
            $this->db->query("SELECT * FROM `items` WHERE `id` IN (SELECT item_id from order_items where order_id = $id)");
            $result = $this->db->resultSet();
            return $result;
        }
    }
