<?php
    class Orders {
        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        /* Test (database and table needs to exist before this works)
        */
        public function get() {
            try {
                //code...
                $this->db->query("SELECT * FROM orders");

                $result = $this->db->resultSet();

                return $result;
            } catch (\Throwable $th) {
                //throw $th;
                return $th;
            }
        }

        public function getByID($id = null) {

            try {
                //code...                
                if(empty($id)) {
                    return '';
                }
                $this->db->query("SELECT * FROM orders WHERE id = $id");
                $result = $this->db->resultSet();
                return $result;
            } catch (\Throwable $th) {
                //throw $th;
                return $th;
            }
        }
    }
