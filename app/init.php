<?php


define('DB_HOST', $_ENV['DB_HOST']); //Add your db host
define('DB_USER', $_ENV['DB_USERNAME']); // Add your DB root
define('DB_PASS', $_ENV['DB_PASSWORD']); //Add your DB pass
define('DB_NAME', $_ENV['DB_DATABASE']); //Add your DB Name

define('PATH_MODEL',  dirname(__FILE__).'/m/');
define('PATH_VIEW',  dirname(__FILE__).'/v/');
define('PATH_CONTROLLER',  dirname(__FILE__).'/c/');

define('DEFAULT_CONTROLLER',  'Home');


//Require libraries from folder libraries
require_once 'lib/Core.php';
require_once 'lib/Controller.php';
require_once 'lib/Database.php';

//Instantiate core class
$init = new Core();