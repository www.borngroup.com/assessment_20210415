<?php
class Rest extends Controller {
    public function __construct() {
        $this->custModel = $this->model('Customers');
        $this->itemModel = $this->model('Items');
        $this->orderModel = $this->model('Orders');
    }

    public function index() {
        echo json_encode(array('status'=>'200 OK'));
    }

    public function order($field = '',$params  ='') {

        try {
            //code...
            if(empty($params)){
                die(json_encode(array('status'=>'400 Bad Request')));
            }
            $orders_data = $this->orderModel->getByID($params);
            $orders = (empty($orders_data[0]))?array():$orders_data[0];    

            $customer_id = $orders->order_by;        
            $data['order_'.$field]=$orders->id;
            $data['order_date']=$orders->created_at;
            $data['customer']= $this->custModel->getCustByID($customer_id);
            $data['items']= $this->itemModel->getItemsByOrderID($params);
            echo json_encode($data);

        } catch (\Throwable $th) {
            //throw $th;
            echo json_encode($th);
        }
    }
}