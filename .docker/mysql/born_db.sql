-- Adminer 4.7.8 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

-- CREATE DATABASE `born` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `born`;

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `customers` (`id`, `name`, `email`, `tel`, `created_at`, `updated_at`) VALUES
(1,	'cust1',	'cust1@cust1.com',	'1122334455',	'2021-04-15 15:48:05',	'2021-04-15 15:48:05'),
(2,	'cust2',	'cust2@cust2.com',	'1122334455',	'2021-04-15 15:48:05',	'2021-04-15 15:48:05');

DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `uom` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `items` (`id`, `name`, `qty`, `uom`, `created_at`, `updated_at`) VALUES
(1,	'shoe',	1,	'pair',	'2021-04-15 16:46:10',	'2021-04-15 15:48:05'),
(2,	'socks',	1,	'pair',	'2021-04-15 16:53:37',	'2021-04-15 15:48:05'),
(3,	'chopsticks',	1,	'pair',	'2021-04-15 16:46:10',	'2021-04-15 15:48:05');

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_by` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `orders` (`id`, `order_by`, `created_at`, `updated_at`) VALUES
(1,	1,	'2021-04-15 16:46:10',	'2021-04-15 15:48:05'),
(2,	2,	'2021-04-15 17:00:40',	'2021-04-15 15:48:05');

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `order_items` (`id`, `order_id`, `item_id`, `created_at`, `updated_at`) VALUES
(4,	1,	1,	'2021-04-15 16:59:18',	'2021-04-15 16:59:18'),
(5,	1,	2,	'2021-04-15 16:59:46',	'2021-04-15 16:59:46'),
(6,	2,	3,	'2021-04-15 17:01:00',	'2021-04-15 17:01:00');

-- 2021-04-15 17:28:36
